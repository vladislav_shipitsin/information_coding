package main.noise;

public class WhiteNoise {
    public static String impose(String s,int[] indexs){
        StringBuffer sb = new StringBuffer(s);
        char buf = ' ';
        int index = 0;
        for (int i = 0; i < indexs.length; i++) {
            index = indexs[i];
            buf = sb.charAt(index);
            if(buf == '0'){
                sb.setCharAt(index,'1');
            }else{
                sb.setCharAt(index,'0');
            }
        }
        return sb.toString();
    }
    public static String impose(String s,int index){
        StringBuffer sb = new StringBuffer(s);
        if(sb.charAt(index) == '0'){
            sb.setCharAt(index,'1');
        }else{
            sb.setCharAt(index,'0');
        }
        return sb.toString();
    }

    public static void imposeOne(int[] word,int index){
        if(word[index] == 0){
            word[index] = 1;
        }else{
            word[index] = 0;
        }
    }
}
