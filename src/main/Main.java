package main;
/*
import main.adama.CoderAdamara;
import main.haming.DecoderHaming;
import main.helpClasses.Convertor;
import main.helpClasses.Operations;
import main.noise.WhiteNoise;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int menu = 0;
        Scanner scanner = new Scanner(System.in);
        // отправленое сообщение
        System.out.print("Enter word: ");
        String sms = scanner.nextLine();
        System.out.println("Word: " + sms);
        System.out.println();

        // конвертируем в поток битов
        String code = Convertor.convertStringToCode(sms);
        System.out.println("Convert word in byte...");
        System.out.print("Press 1 for get info: ");
        menu = scanner.nextInt();
        if (menu == 1) {
            System.out.println("code lenght: " + code.length());
            System.out.println("code: " + code);
        }
        System.out.println();

        // кодируем с помощью Кода Хэмминга
        System.out.println("Coder Haming begin...");

//        Coder coderHaming = new CoderHaming(code);
        Coder coderHaming = new CoderAdamara(code.length());
        String hamingCode = coderHaming.codingSms(code);

        System.out.println("Coder Haming finish...");
        System.out.print("Press 1 for get info: ");
        menu = scanner.nextInt();
        if (menu == 1) {
            System.out.println(hamingCode);
            System.out.println("d: " + coderHaming.getD());
            System.out.println("s: " + coderHaming.getS());
            System.out.println("n: " + coderHaming.getN());
            Operations.printMatrix(coderHaming.getMatrixG(), "matrix G: ");
        }
        System.out.println();
        // Накладываем помехи
        System.out.print("Enter index (<" + hamingCode.length() + ") for white noise: ");
        int index = scanner.nextInt();
        String codeWithNoise = WhiteNoise.impose(hamingCode, index);
        System.out.println("White noise finish...");
        System.out.print("Press 1 for get info: ");
        menu = scanner.nextInt();
        if (menu == 1) {
            System.out.println("befor: " + hamingCode);
            System.out.println("noise: " + index);
            System.out.println("after: " + codeWithNoise);
        }
        System.out.println();

        //Декодируем и исправляем ошибку
        System.out.println("Decoder Haming begin...");

        DecoderHaming decoderHaming = new DecoderHaming(coderHaming);
        String resultDecoder = decoderHaming.decodeSms(codeWithNoise);

        System.out.println("Decoder Haming finish...");
        System.out.print("Press 1 for get info: ");
        menu = scanner.nextInt();
        if (menu == 1) {
            System.out.println("d: " + decoderHaming.getD());
            System.out.println("s: " + decoderHaming.getS());
            System.out.println("n: " + decoderHaming.getN());
            Operations.printMatrix(decoderHaming.getMatrixH(), "matrix H: ");
            Operations.printArr(decoderHaming.getSyndrome(), "syndrome: ");
        }

        // Конвертируем поток битов обратно в текст
        String resultSms = Convertor.convertCodeToString(resultDecoder);

        // Полученное сообщение
        System.out.println("Get word: " + resultSms);

    }

}*/
