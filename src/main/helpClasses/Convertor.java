package main.helpClasses;

public class Convertor {

    public static String convertStringToCode(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            sb.append(convertCharToCode(s.charAt(i)));
        }
        return sb.toString();
    }

    private static String convertCharToCode(Character c) {
        StringBuffer sb = new StringBuffer();
        sb.append(Integer.toBinaryString(c));
        while (sb.length() < 8) sb.insert(0, '0');
        return sb.toString();
    }

    public static String convertCodeToString(String code) {
        StringBuffer sb = new StringBuffer(code);
        StringBuffer result = new StringBuffer();
        if (sb.length() % 8 != 0) {
            return "ERROR CONVERTOR LENGHT";
        }
        for (int i = 0; i < sb.length(); i += 8) {
            result.append(convertCodeToChar(sb.substring(i, i + 8)));
        }
        return result.toString();
    }

    private static Character convertCodeToChar(String code) {
        return (char) Integer.parseInt(code, 2);
    }

    public static String convertArrayToString(int[] arr) {
        return null;
    }

    public static int[] convertStringToArr(String s) {
        int[] arr = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            arr[i] = (int) s.charAt(i) - 48;
        }
        return arr;
    }

}
