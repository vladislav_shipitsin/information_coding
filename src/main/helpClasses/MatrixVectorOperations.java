package main.helpClasses;

import java.util.Arrays;

public class MatrixVectorOperations {

    public static int[] multiplyArrToMatrix(final int[] arr,final int[][] matrix){
        if(arr.length != matrix.length){
            throw new IllegalArgumentException();
        }

        final int[] result = new int[matrix[0].length];
        Arrays.fill(result,0);

        for (int j = 0; j < result.length; j++) {

            for (int i = 0; i < arr.length; i++) {
                result[j] += arr[i] * matrix[i][j];
                result[j] %= 2;
            }
        }

        return result;
    }

    public static int[] arrayDifference(final int[] arr1,final int[] arr2){
        if(arr1.length != arr2.length){
            throw new IllegalArgumentException("Размеры массивов не совпадают");
        }
        final int[] arrResult = new int[arr1.length];
        for (int i = 0; i < arrResult.length; i++) {
            arrResult[i] = Math.abs(arr1[i] - arr2[i]) % 2;
        }
        return arrResult;
    }

    public static int[] multiplyArrToMatrixForReadMaller(final int[][] arr,final int[][][] matrix){
        if(arr.length != matrix[0].length){
            throw new IllegalArgumentException();
        }

        final int[] result = new int[matrix[0].length];

throw new IllegalArgumentException("not implemented");
        //return result;
    }
}
