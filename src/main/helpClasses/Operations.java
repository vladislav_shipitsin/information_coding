package main.helpClasses;

public class Operations {

    public static boolean isPowTwo(int n) {
        return ((n & (n - 1)) == 0);
    }

    public static void getColomJ(int[][] matrix, int[] colom, int j) {
        for (int i = 0; i < matrix.length; i++) {
            colom[i] = matrix[i][j];
        }
    }

    public static void printMatrix(int[][] matrix, String s) {
        if (s == null) {
            System.out.println("Matrix: ");
        } else {
            System.out.println(s);
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%5d  ", matrix[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printArr(int[] arr, String s) {
        if (s == null) {
            System.out.println("Array: ");
        } else {
            System.out.println(s);
        }
        for (int el : arr) {
            System.out.printf("%2d  ", el);
        }
        System.out.println();
        System.out.println();
    }
}
