package main.helpClasses;

public class Printer {

    public static void printArr(byte[] arr){
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < arr.length ; i++) {
            s.append(arr[i]);
        }
        System.out.println(s.toString());
    }
    public static void printArr(int[] arr){
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < arr.length ; i++) {
            s.append(arr[i]);
        }
        System.out.println(s.toString());
    }

}
