package normalRealise;

import main.helpClasses.Convertor;
import normalRealise.algoritm.Algorithm ;
import normalRealise.comminicator.Recipient;
import normalRealise.comminicator.Sender;
import normalRealise.comminicator.SimpleRecipient;
import normalRealise.comminicator.SimpleSender;
import normalRealise.noise.Interference;

import java.util.LinkedList;
import java.util.List;

/**
 * Канал связи. Имеет получателя и отправителя, а так же обеспечивает связь между ними.
 */
public class Channel {

    private Algorithm  algoritm;
    private Sender sender; // отправитель сообщений
    private Recipient recipient; // получатель
    private Interference interference; // накладывает помехи

    public Channel(Algorithm  algoritm, Interference interference) {
        this.algoritm = algoritm;
        this.interference = interference;
        sender = new SimpleSender();
        recipient = new SimpleRecipient();
    }

    public String processMessage(final String message) {

        // преобразуем в поток байтов
        String word = Convertor.convertStringToCode(message);

        final List<String> codeWords = sender.send(algoritm, word);
        final List<String> distortedWords = new LinkedList<>();

        for (final String codeWord : codeWords) {
            distortedWords.add(interference.imposeNoise(codeWord));
        }

        final StringBuffer resultInfoWord = new StringBuffer(recipient.process(algoritm, distortedWords));


        // удалим лишние элементы
        final int sizeDeletedElement = resultInfoWord.length() - word.length();

        for (int i = 0; i < sizeDeletedElement; i++) {
            resultInfoWord.deleteCharAt(resultInfoWord.length() - 1);
        }

        return Convertor.convertCodeToString(resultInfoWord.toString());
    }
}
