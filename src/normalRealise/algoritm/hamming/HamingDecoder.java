package normalRealise.algoritm.hamming;

import main.helpClasses.Convertor;
import main.helpClasses.Operations;
import normalRealise.algoritm.Decoder;

import java.util.Arrays;

public class HamingDecoder implements Decoder {
    private final int sizeInfoWord;
    private final int sizeCodeWord;
    private final int[][] matrixH;

    public HamingDecoder(int sizeInfoWord, int sizeCodeWord, int[][] matrixH) {
        this.sizeInfoWord = sizeInfoWord;
        this.sizeCodeWord = sizeCodeWord;
        this.matrixH = matrixH;
    }

    @Override
    public String decoding(String distortedWord) {
        return decodeSms(distortedWord);
    }

    public String decodeSms(String sms) {
        int[] syndrome = new int[sizeCodeWord - sizeInfoWord];

        int[] arrU = Convertor.convertStringToArr(sms);

        // вычисляем синдром
        syndrome = matrixMultiArr(matrixH, arrU);

        // ищем ошибку
        int er = errorSearch(matrixH,syndrome);
        if (er != -1) {
            // исправляем её если она есть
            arrU[er]++;
            arrU[er] %= 2;
        }
        // выбираем исходное слово
        int[] result = getWord(arrU, sizeInfoWord);

        // преобразуем в String
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(result[i]);
        }

        return sb.toString();
    }

    private int errorSearch(int[][] matrixH,final int[] syndrome) {
        int[] temp = new int[matrixH.length];
        for (int j = 0; j < matrixH[0].length; j++) {
            Operations.getColomJ(matrixH, temp, j);
            if (Arrays.equals(syndrome, temp)) {
                return j;
            }
        }
        return -1;
    }

    private int[] getWord(int[] arrU, int d) {
        int[] arrX = new int[d];
        for (int i = 0; i < d; i++) {
            arrX[i] = arrU[i];
        }
        return arrX;
    }

    private int[] matrixMultiArr(int[][] matrixH, int[] arrU) {
        final int[] syndrome = new int[matrixH.length];
        for (int i = 0; i < matrixH.length; i++) {
            for (int j = 0; j < matrixH[0].length; j++) {
                syndrome[i] += matrixH[i][j] * arrU[j];
            }
            syndrome[i] %= 2;
        }
        return syndrome;
    }
}
