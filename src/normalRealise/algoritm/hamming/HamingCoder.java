package normalRealise.algoritm.hamming;

import main.helpClasses.Operations;
import normalRealise.algoritm.Coder;

import java.math.BigInteger;

public class HamingCoder implements Coder {

    private final int sizeInfoWord;
    private final int sizeCodeWord;
    private final int[][] matrixH;
    private final int[][] matrixG;

    public HamingCoder(final int sizeInfoWord, final int sizeCodeWord) {
        this.sizeInfoWord = sizeInfoWord;
        this.sizeCodeWord = sizeCodeWord;
        matrixH = initMatrixH();
        matrixG = getMatrixG(matrixH);
    }

    public static int[] arrMultiMatrix(int[] arr, int[][] matrix) {
        if (arr.length != matrix.length) {
            throw new IllegalArgumentException("Умножение не возможно с такими размерами");
        }
        int[] result = new int[matrix[0].length];
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < arr.length; i++) {
                result[j] += arr[i] * matrix[i][j];
            }
            result[j] %= 2;
        }
        return result;
    }

    public static String arrToString(int[] arr) {
        StringBuffer s = new StringBuffer();
        for (int el : arr) {
            s.append(el);
        }
        return s.toString();
    }

    public int[][] getMatrixH() {
        return matrixH;
    }

    @Override
    public String coding(String infoWord) {
        int[] arrX = new int[infoWord.length()];
        for (int i = 0; i < infoWord.length(); i++) {
            arrX[i] = (int) infoWord.charAt(i) - 48;
        }

        int[] arrU = arrMultiMatrix(arrX, matrixG);

        return arrToString(arrU);
    }

    private int[][] initMatrixH() {
        final int s = sizeCodeWord - sizeInfoWord;
        final int n = sizeCodeWord;

        int[][] matrixH = new int[s][n];
        int k = 0;
        for (int j = 0; j < n; j++) {
            if (!Operations.isPowTwo(j + 1)) {
                BigInteger bigInteger = new BigInteger("" + (j + 1));
                setColom(matrixH, bigInteger.toString(2), k);
                k++;
            }
        }
        for (int j = 0; j < s; j++) {
            matrixH[s - j - 1][n - j - 1] = 1;
        }
        return matrixH;
    }

    private int[][] getMatrixG(int[][] matrixH) {
        int n = matrixH[0].length;
        int s = matrixH.length;
        int d = n - s;
        int[][] matrixG = new int[d][n];
        for (int i = 0; i < d; i++) {
            matrixG[i][i] = 1;
        }
        for (int i = 0; i < d; i++) {
            for (int j = d; j < n; j++) {
                matrixG[i][j] = matrixH[j - d][i];
            }
        }
        return matrixG;
    }

    private void setColom(int[][] matrix, String colom, int j) {
        int r = matrix.length - colom.length();
        StringBuffer stringBuffer = new StringBuffer();
        while (r != 0) {
            stringBuffer.append('0');
            r--;
        }
        stringBuffer.append(colom);
        colom = stringBuffer.toString();

        for (int i = 0; i < matrix.length; i++) {
            matrix[i][j] = (int) colom.charAt(i) - 48;
        }
    }
}
