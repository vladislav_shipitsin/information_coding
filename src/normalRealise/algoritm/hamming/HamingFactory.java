package normalRealise.algoritm.hamming;

import normalRealise.algoritm.Algorithm;
import normalRealise.algoritm.AlgoritmFactory;

public class HamingFactory implements AlgoritmFactory {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final int SIZE_CODE_WORD = 3;
    public static final int SIZE_INFO_WORD = 1;

   /* public static void main(String[] args) {
        final int T = 100;

        final double[] arrW = new double[20];
        for (int i = 1; i < arrW.length; i++) {
            arrW[i] = arrW[i - 1] + 0.05;
        }
        final double[] arrP = new double[20];
        for (int i = 1; i < arrP.length; i++) {
            arrP[i] = arrP[i - 1] + 0.025;
        }

        final double[][] res = new double[arrP.length][arrW.length];

        // шапка

        System.out.printf("   p/w  ");
        for (int i = 0; i < arrW.length; i++) {
            System.out.printf(" |%.2f ",arrW[i]);
        }


        for (int p = 0; p < arrP.length; p++) {
            System.out.println();
            System.out.printf(" |%.3f ",arrP[p]);
            for (int w = 0; w < arrW.length; w++) {

                res[p][w] = getBestSizeCodeWord(T,arrP[p],arrW[w]);

                if(res[p][w] < arrW[w]){

                    System.out.printf(ANSI_RED + " |%.2f " + ANSI_RESET,res[p][w]);
                }else{
                    System.out.printf(ANSI_RESET + " |%.2f ",res[p][w]);
                }

            }
        }
    }*/

    private static int getLenghtCodingWord(int d) {
        int n = 0;
        int s = 2;
        while (d > (Math.pow(2, s) - s - 1)) {
            s++;
        }
        n = d + s;
        return n;
    }

    /**
     * @param T Пропускная способность алгоритма
     * @param p вероятность изменения бита
     * @param w максимально допустимая погрешность.
     * @return
     */
    private static double getBestSizeCodeWord(final int T, final double p, final double w) {
        int bestN = -1;
        double bestValue = -5;
        for (int n = 3; n <= T; n++) {
            final double a = Math.pow((1 - p), (n - 1));
            final double b = 1 - p + n * p;
            final double res = a * b;

            if (res > bestValue) {
                bestValue = res;
                bestN = n;
            }

        }
        if (bestN == -1) {
            return 0;
        }
        if(bestN != 3){
            throw new IllegalArgumentException();
        }
        /*System.out.println("bestN = " + bestN);
        System.out.println("best value = " + bestValue);*/
        return bestValue;
    }

    public static void main(String[] args) {

        double step = 0.025;
        double bound = 0.5;
        int size = (int) (bound / step) + 1;
        final double[] arrP = new double[size];

        final double[] arrW = new double[size];

        final double[] arrPowW = new double[size];

        for (int i = 1; i < arrP.length; i++) {
            arrP[i] = arrP[i - 1] + step;

        }

        for (int i = 0; i < arrW.length; i++) {
            double p = arrP[i];

//            arrW[i] = (1 - p) * (1 - p) * (1 + 2 * p);
            arrW[i] = getBestSizeCodeWord(100,arrP[i],arrW[i]);

            arrPowW[i] = Math.pow(arrW[i],8*3);
        }

        printArr(arrP);
        printArr(arrW);
        printArr(arrPowW);
    }

    public static void printArr(final double[] arr){
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.printf("  %.3f|",arr[i]);
        }
        System.out.println();
    }

    public Algorithm getAlgoritm(int sizeInfoWord) {

        final HamingCoder coder = new HamingCoder(SIZE_INFO_WORD, SIZE_CODE_WORD);
        final int[][] matrixH = coder.getMatrixH();
        final HamingDecoder decoder = new HamingDecoder(SIZE_INFO_WORD, SIZE_CODE_WORD, matrixH);

        return new HamingAlgorithm(SIZE_INFO_WORD, SIZE_CODE_WORD, coder, decoder);
    }

    @Override
    public Algorithm getAlgoritm(int T, double p, double w) {
        final HamingCoder coder = new HamingCoder(SIZE_INFO_WORD, SIZE_CODE_WORD);
        final int[][] matrixH = coder.getMatrixH();
        final HamingDecoder decoder = new HamingDecoder(SIZE_INFO_WORD, SIZE_CODE_WORD, matrixH);

        return new HamingAlgorithm(SIZE_INFO_WORD, SIZE_CODE_WORD, coder, decoder);
    }
}
