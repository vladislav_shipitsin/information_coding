package normalRealise.algoritm;

public interface Decoder {
    /**
     * Разкодировать слово
     * @param distortedWord - искаженное кодовое слово.
     * @return - информационное слово.
     */
    String decoding(String distortedWord);
}
