package normalRealise.algoritm;

public interface AlgoritmFactory {
    Algorithm  getAlgoritm(final int sizeInfoWord);
    Algorithm  getAlgoritm(final int T,final double p, final double w);
}
