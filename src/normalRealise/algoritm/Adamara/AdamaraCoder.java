package normalRealise.algoritm.Adamara;

import normalRealise.algoritm.Coder;
import normalRealise.common.Convertor;

public class AdamaraCoder implements Coder {
    private static final int[][] smallMatrix = {{1, 1}, {1, -1}};
    private final int k;
    private final int[][] matrixAdamara;

    public AdamaraCoder(int k) {
        this.k = k;
        matrixAdamara = fillMatrixAdamara(k);
    }

    @Override
    public String coding(final String infoWord) {
        // вычислить номер получается
        final int indexCodeWord = Integer.parseInt(infoWord, 2);
        return Convertor.convertArrayToString(matrixAdamara[indexCodeWord]);
    }

    public int[][] getMatrixAdamara() {
        return matrixAdamara;
    }

    private int[][] fillMatrixAdamara(final int k) {

        int[][] resultMatrix = smallMatrix;

        for (int i = 1; i < k; i++) {
            resultMatrix = getLargeMatrix(resultMatrix);
        }

        int[][] matrixH = new int[resultMatrix.length][resultMatrix.length - 1];

        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 1; j < resultMatrix.length; j++) {
                if (resultMatrix[i][j] == 1) {
                    matrixH[i][j - 1] = 0;
                    continue;
                }
                if (resultMatrix[i][j] == -1) {
                    matrixH[i][j - 1] = 1;
                    continue;
                }
                throw new IllegalArgumentException("В матрице должны быть только 1 и -1");
            }
        }
        return matrixH;
    }

    /**
     * Увеличить размерность матрицы в 2 раза
     *
     * @param matrix
     * @return
     */
    private int[][] getLargeMatrix(final int[][] matrix) {
        final int sizeMatrix = 2 * matrix.length;
        final int[][] result = new int[sizeMatrix][sizeMatrix];

        // первая четверть
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                result[i][j] = matrix[i][j];
            }
        }
        // вторая четверть
        for (int i = 0; i < matrix.length; i++) {
            for (int j = matrix.length; j < result.length; j++) {
                result[i][j] = matrix[i][j - matrix.length];
            }
        }
        // третья четверть
        for (int i = matrix.length; i < result.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                result[i][j] = matrix[i - matrix.length][j];
            }
        }
        // четвертая четверть
        for (int i = matrix.length; i < result.length; i++) {
            for (int j = matrix.length; j < result.length; j++) {
                result[i][j] = -matrix[i - matrix.length][j - matrix.length];
            }
        }
        return result;
    }

}
