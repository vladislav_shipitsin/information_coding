package normalRealise.algoritm.Adamara;

import normalRealise.algoritm.Algorithm;
import normalRealise.algoritm.AlgoritmFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdamaraFactory implements AlgoritmFactory {
    // todo сделать мапу из длины слова в алгоритм, чтобы избежать перерасчетов

    private static final List<Integer> respectValuesN = new ArrayList<>();
    private static final Map<Integer, Integer> nToK = new HashMap<>();
    private static final Map<Integer, Integer> nToError = new HashMap<>();
    private static final int MAX_N = 16383;
    private static final int MIN_N = 3;

    static {
        respectValuesN.add(3);
        respectValuesN.add(7);
        respectValuesN.add(15);
        respectValuesN.add(31);
        respectValuesN.add(63);
        respectValuesN.add(127);
        respectValuesN.add(255);
        respectValuesN.add(511);
        respectValuesN.add(1023);
        respectValuesN.add(2047);
        respectValuesN.add(4095);
        respectValuesN.add(8191);
        respectValuesN.add(16383);

        nToK.put(3, 2);
        nToK.put(7, 3);
        nToK.put(15, 4);
        nToK.put(31, 5);
        nToK.put(63, 6);
        nToK.put(127, 7);
        nToK.put(255, 8);
        nToK.put(511, 9);
        nToK.put(1023, 10);
        nToK.put(2047, 11);
        nToK.put(4095, 12);
        nToK.put(8191, 13);
        nToK.put(16383, 14);

        nToError.put(3, 0);
        nToError.put(7, 1);
        nToError.put(15, 3);
        nToError.put(31, 7);
        nToError.put(63, 15);
        nToError.put(127, 63);
        nToError.put(255, 127);
        nToError.put(511, 255);
        nToError.put(1023, 511);
        nToError.put(2047, 1023);
        nToError.put(4095, 2047);
        nToError.put(8191, 4095);
        nToError.put(16383, 8191);
    }

    private static int getLenghtCodingWord(final int sizeInfoWord) {
        return (int) (Math.pow(2, sizeInfoWord) - 1);
    }

    private static int getCorrectBoundN(final int T) {
        if (T < MIN_N) {
            throw new IllegalArgumentException("Сличком мальнькое T");
        }
        if (MAX_N < T) return MAX_N;
        for (int i = 1; i < respectValuesN.size(); i++) {
            if (respectValuesN.get(i) > T) {
                return respectValuesN.get(i - 1);
            }
        }
        throw new IllegalArgumentException("Ты опять накосячил в логике");
    }

    private static int getBestN(final int T, final double p, final double w) {

        if(T > 30) return 127;

        int boundN = getCorrectBoundN(T);

        double bestProbability = -5;
        int bestN = -1;


        for (Integer n : respectValuesN) {

            if (n > boundN) {
                return bestN;
            }

            double sum = 0.0;
            for (int e = 0; e <= nToError.get(n); e++) {
                sum += bernuli(n, e, p);
            }
            if (sum >= 1 - w) {
                return n;
            }
            if(bestProbability < sum){
                bestProbability = sum;
                bestN = n;
            }
        }

        if(bestN == -1){
            throw new IllegalArgumentException("Чтото не так");
        }
        return bestN;
    }

    private static double bernuli(int n, int k, double p) {
        return getCombination(k, n) * Math.pow(p, k) * Math.pow((1 - p), (n - k));
    }

    public static int getCombination(final int r, final int m) {
        return factorial(m) / factorial(r) / factorial(m - r);
    }

    public static int factorial(final int n) {
        if(n < 0 ) throw new IllegalArgumentException();
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }

    @Override
    public Algorithm getAlgoritm(int T, double p, double w) {
        final int sizeCodeWord = getBestN(T,p,w);
        final int sizeInfoWord = nToK.get(sizeCodeWord);

        final AdamaraCoder coder = new AdamaraCoder(sizeInfoWord);
        final int[][] matrixAdamara = coder.getMatrixAdamara();
        final AdamaraDecoder decoder = new AdamaraDecoder(sizeInfoWord, sizeCodeWord, matrixAdamara);

        return new AdamaraAlgorithm(sizeInfoWord, sizeCodeWord, coder, decoder);
    }

    public Algorithm getAlgoritm(final int sizeInfoWord) {
        final int sizeCodeWord = getLenghtCodingWord(sizeInfoWord);

        final AdamaraCoder coder = new AdamaraCoder(sizeInfoWord);
        final int[][] matrixAdamara = coder.getMatrixAdamara();
        final AdamaraDecoder decoder = new AdamaraDecoder(sizeInfoWord, sizeCodeWord, matrixAdamara);

        return new AdamaraAlgorithm(sizeInfoWord, sizeCodeWord, coder, decoder);
    }
}
