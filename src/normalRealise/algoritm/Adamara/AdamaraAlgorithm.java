package normalRealise.algoritm.Adamara;

import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.Coder;
import normalRealise.algoritm.Decoder;

public class AdamaraAlgorithm extends Algorithm  {
    AdamaraAlgorithm(int sizeInfoWord, final int sizeCodeWord, final Coder coder, final Decoder decoder) {
        super(sizeInfoWord, sizeCodeWord, coder, decoder);
    }
}
