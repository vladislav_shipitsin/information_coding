package normalRealise.algoritm.Adamara;

import main.helpClasses.Convertor;
import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.Decoder;

import static normalRealise.common.Convertor.binar;

public class AdamaraDecoder implements Decoder {
    private final int sizeInfoWord;
    private final int sizeCodeWord;

    private final int[][] matrixAdamara;

    public AdamaraDecoder(int sizeInfoWord, int sizeCodeWord, int[][] matrixAdamara) {
        this.sizeInfoWord = sizeInfoWord;
        this.sizeCodeWord = sizeCodeWord;
        this.matrixAdamara = matrixAdamara;
    }

    @Override
    public String decoding(String distortedWord) {
        int[] wordArr = Convertor.convertStringToArr(distortedWord);

        int minSize = 10000;
        int indexMin = -1;

        for (int i = 0; i < matrixAdamara.length; i++) {
            int[] temp = matrixAdamara[i];
            int count = 0;

            for (int j = 0; j < temp.length; j++) {

                count += (temp[j] + wordArr[j]) % 2;
            }

            if (count < minSize) {
                minSize = count;
                indexMin = i;
            }
        }

        // вытащить индекс

        StringBuffer binar = new StringBuffer(binar(indexMin));
        while (binar.length() < sizeInfoWord) {
            binar.insert(0, "0");
        }
        return binar.toString();
    }
}
