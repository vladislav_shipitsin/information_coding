package normalRealise.algoritm;

public abstract class Algorithm  {
    protected final int sizeInfoWord;
    protected final int sizeCodeWord;
    protected final Decoder decoder;
    protected final Coder coder;

    public Algorithm (int sizeInfoWord, int sizeCodeWord, Coder coder, Decoder decoder) {
        this.sizeInfoWord = sizeInfoWord;
        this.sizeCodeWord = sizeCodeWord;
        this.coder = coder;
        this.decoder = decoder;
    }

    public Algorithm (int sizeInfoWord, Coder coder, Decoder decoder) {
        this(sizeInfoWord, 0, coder, decoder);
    }

    public int getSizeInfoWord() {
        return sizeInfoWord;
    }

    public int getSizeCodeWord() {
        return sizeCodeWord;
    }

    public Coder getCoder() {
        return coder;
    }

    public Decoder getDecoder() {
        return decoder;
    }

    @Override
    public String toString() {
        return "algoritm : " + getClass().getSimpleName();
    }
}
