package normalRealise.algoritm;

public interface Coder {

    /**
     * Закодировать слово.
     * @param infoWord - информационное слово.
     * @return кодовое слово.
     */
    String coding(String infoWord);
}
