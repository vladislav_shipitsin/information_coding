package normalRealise.algoritm.read_maller;

import main.helpClasses.MatrixVectorOperations;
import normalRealise.algoritm.Decoder;
import normalRealise.common.Convertor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Декодирование идет с конца.
 * I = (I0,...,Ir)
 * Берем Ir у него берем i_(k-1)
 */
public class ReadMallerDecoder implements Decoder {
    final int m;
    final int r;
    final int k;
    final int n;

    private final int[][][] matrixG;

    public ReadMallerDecoder(int k, int n, final int r, final int m, int[][][] matrixG) {
        this.m = m;
        this.r = r;
        this.k = k;
        this.n = n;
        this.matrixG = matrixG;
    }

    @Override
    public String decoding(String distortedWord) {

        int[] codeWord = Convertor.convertStringToArr(distortedWord);

        final int[] infoWord = new int[k];
        Arrays.fill(infoWord,-1);
        int currentIndexForInfoWord = infoWord.length - 1;

        final int[][] arrI = splitArray(infoWord, matrixG);

        if (arrI.length != matrixG.length) {
            throw new IllegalArgumentException("Разбиение произошло не корректно");
        }

        // начинаем с последней состовляющей матрицы G
        for (int s = matrixG.length - 1; s > 0; s--) {

            final int[] currentI = arrI[s];
            final int[][] matrixGi = matrixG[s];

            // 1) сколько сумм 2^(m-s)
            final int numSums = (int) Math.pow(2, (m - s));

            // количество слагаемых
            final int numTerms = (int) Math.pow(2, s);
            // начинаем с последнего элемента в блоке I
            for (int j = currentI.length - 1; j >= 0; j--) {

                final int[] copyShafleCodeWord = getShafleCodeWordsIndex(codeWord, matrixGi[j], numTerms);

                int currentIndex = 0;
                // если отрицательный значит было больше 0, если положительный значит 1. Иначе ошибка.
                int determinateResult = 0;
                for (int i = 0; i < numSums; i++) {

                    int tempSum = 0;

                    for (int t = 0; t < numTerms; t++) {
                        tempSum += copyShafleCodeWord[currentIndex++];
                        tempSum %= 2;
                    }

                    if (tempSum == 1) {
                        determinateResult++;
                    } else if (tempSum == 0) {
                        determinateResult--;
                    } else {
                        throw new IllegalArgumentException("не допустимое значение временной суммы");
                    }
                }

                /*if (determinateResult == 0) {
                    throw new IllegalArgumentException("Обнаружить значение переменной не представляется возможным");
                }*/

                currentI[j] = determinateResult > 0 ? 1 : 0;
                infoWord[currentIndexForInfoWord--] = currentI[j];
            }


            // после того как посчитали блок надо пересчитать информациооное слово
            final int[] consequence = MatrixVectorOperations.multiplyArrToMatrix(currentI,matrixGi);
            codeWord = MatrixVectorOperations.arrayDifference(codeWord,consequence);
        }

        // мажоритарный способ для определения i0
        infoWord[currentIndexForInfoWord] = majorityWay(codeWord);

        return Convertor.convertArrayToString(infoWord);
    }

    public List<Integer> getColomWhereOne(final int line,final int[] arrGsi){
        List<Integer> indexes = new LinkedList<>();

        for (int i = 0; i < arrGsi.length; i++) {
            if(arrGsi[i] == 1){
                indexes.add(i);
            }
        }
        return indexes;
    }
    private int[][] splitArray(final int[] arr, final int[][][] matrixG) {
        final int[][] res = new int[matrixG.length][];

        int currentIndex = 0;
        for (int i = 0; i < matrixG.length; i++) {
            final int[] currentI = new int[matrixG[i].length];

            for (int j = 0; j < currentI.length; j++) {
                currentI[j] = arr[currentIndex];
                currentIndex++;
            }
            res[i] = currentI;
        }
        return res;
    }

    /**
     * Зашафлить таким образом чтобы еденицы в стоке j у матрицы GI стояли на каждом numTerms месте
     */
    private int[] getShafleCodeWordsIndex(final int[] codeWords, final int[] arrGsj, final int numTerms) {
        final int[] result = Arrays.copyOf(codeWords, codeWords.length);

        List<Integer> indexesNeedOne = new LinkedList<>();
        List<Integer> indexesExcessOne = new LinkedList<>();

        for (int i = 0; i < arrGsj.length; i++) {
            int elem = arrGsj[i];

            if (elem == 0) {
                if (((i + 1) % numTerms) == 0) { // ноль есть а нужен 1
                    indexesNeedOne.add(i);
                }
                continue;
            }
            if (elem == 1) {
                if (((i + 1) % numTerms) != 0) { // 1 есть а должен быть 0
                    indexesExcessOne.add(i);
                }
                continue;
            }
            throw new IllegalArgumentException("Массив должен содержать только 0 и 1");
        }

        if (indexesExcessOne.size() != indexesNeedOne.size()) {
            throw new IllegalArgumentException("не правильно разбил");
        }

        while (!indexesExcessOne.isEmpty() && !indexesNeedOne.isEmpty()) {
            final int indexNeed = indexesNeedOne.remove(0);
            final int indexExcess = indexesExcessOne.remove(0);

            final int tempElem = result[indexNeed];
            result[indexNeed] = result[indexExcess];
            result[indexExcess] = tempElem;
        }

        return result;
    }

    private int majorityWay(final int[] arr){
        int determinateValue = 0;
        for (int j : arr) {
            determinateValue += j == 0 ? -1 : 1;
        }
       /* if (determinateValue == 0) {
            throw new IllegalArgumentException("Обнаружить значение переменной не представляется возможным");
        }*/
        return determinateValue > 0 ? 1 : 0;
    }
}
