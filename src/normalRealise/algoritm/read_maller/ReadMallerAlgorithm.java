package normalRealise.algoritm.read_maller;

import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.Coder;
import normalRealise.algoritm.Decoder;

public class ReadMallerAlgorithm extends Algorithm  {


    public ReadMallerAlgorithm(int sizeInfoWord, int sizeCodeWord, Coder coder, Decoder decoder) {
        super(sizeInfoWord, sizeCodeWord, coder, decoder);
    }

    public static void printMatrixG(final int[][][] matrixG) {

        for (int r = 0; r < matrixG.length; r++) {
            System.out.println("G" + r + ":");
            for (int i = 0; i < matrixG[r].length; i++) {


                for (int j = 0; j < matrixG[r][i].length; j++) {

                    System.out.print(matrixG[r][i][j] + " ");
                }
                System.out.println();
            }


        }

    }
}
