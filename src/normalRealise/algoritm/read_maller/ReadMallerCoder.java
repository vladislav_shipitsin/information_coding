package normalRealise.algoritm.read_maller;

import main.helpClasses.MatrixVectorOperations;
import normalRealise.algoritm.Coder;
import normalRealise.common.Convertor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ReadMallerCoder implements Coder {

    final int m;
    final int r;
    final int k;
    final int n;

    private final int[][][] matrixG;
    private final int[][] simpleMatrixG;

    public ReadMallerCoder(int k, int n, final int r, final int m) {
        this.m = m;
        this.r = r;
        this.k = k;
        this.n = n;
        matrixG = initMatrixG(r, m);
        simpleMatrixG = getSimpleMatrixG();
    }

    static boolean nextSet(int[] a, int n, int m) {
        int k = m;
        for (int i = k - 1; i >= 0; --i)
            if (a[i] < n - k + i + 1) {
                ++a[i];
                for (int j = i + 1; j < k; ++j)
                    a[j] = a[j - 1] + 1;
                return true;
            }
        return false;
    }

    static List<int[]> getAllVarians(final int m, final int n) {
        List<int[]> result = new LinkedList<>();

        int[] a = new int[m];
        for (int i = 0; i < m; i++) {
            a[i] = i + 1;
        }
       /* int[] newA = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            newA[i] = a[i] - 1;
        }
        result.add(newA);*/
        result.add(Arrays.copyOf(a, a.length));
        if (n >= m) {
            while (nextSet(a, n, m)) {
               /* newA = new int[a.length];
                for (int i = 0; i < a.length; i++) {
                    newA[i] = a[i] - 1;
                }
                result.add(newA);*/
                result.add(Arrays.copyOf(a, a.length));
            }
        }

        for (int[] arr : result) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] -= 1;
            }
        }
        return result;
    }



    // посроение порождающей матрицы по параметрам

    private int[][] getSimpleMatrixG() {
        final int[][] result = new int[k][n];

        int currentIndex = 0;
        for (int i = 0; i < matrixG.length; i++) {
            for (int j = 0; j < matrixG[i].length; j++) {
                System.arraycopy(matrixG[i][j], 0, result[currentIndex], 0, matrixG[i][j].length);
                currentIndex++;
            }
        }
        return result;
    }

    @Override
    public String coding(String infoWord) {
        // получить вектор и умножить его слево на матрицу G
        final int[] arrInfoWord = Convertor.convertStringToArr(infoWord);

        final int[] arrCodeWord = MatrixVectorOperations.multiplyArrToMatrix(arrInfoWord, simpleMatrixG);

        return Convertor.convertArrayToString(arrCodeWord);
    }

    public int[][][] initMatrixG(final int r, final int m) {
        int[][][] matrixG = new int[r + 1][][];
        /**
         * n = 2^m
         * k = 1 + C(1,m) + ... + C(r,m). Где С(i,j) число сочетаний
         * G = (G0,G1,...Gr)
         * G0 = (1,1,1,1,...1) m штук
         * G1 = все возможные матрицы двоичных представлений чисел (m*n)
         */
       /* final int k = getK(r, m);
        final int n = (int) Math.pow(2, m);*/

        //G0
        int[][] matrixG0 = new int[1][n];
        Arrays.fill(matrixG0[0], 1);
        matrixG[0] = matrixG0;

        // взять число от 0 до n.
        // преоброзовать в массив битов двоичного представления
        // добавить нули в начало до m

        // G1
        int[][] matrixG1 = new int[m][n];
        for (int j = 0; j < n; j++) {
            StringBuilder s = new StringBuilder(Integer.toBinaryString(j));

            while (s.length() < m) {
                s.insert(0, "0");
            }

            for (int i = 0; i < m; i++) {
                matrixG1[i][j] = Integer.parseInt(s.charAt(i) + "");
            }
        }

        matrixG[1] = matrixG1;

        // Gi - все возможные наборы по i из G1
        for (int numLines = 2; numLines <= r; numLines++) {
            List<int[]> listIndexes = getAllVarians(numLines, m);

            int[][] matrixGi = new int[listIndexes.size()][n];

            for (int i = 0; i < listIndexes.size(); i++) {
                final int[] arrIndexes = listIndexes.get(i);

                if (arrIndexes.length != numLines) {
                    throw new IllegalArgumentException();
                }

                matrixGi[i] = getMultiplyLine(matrixG1, arrIndexes);
            }
            matrixG[numLines] = matrixGi;
        }


        return matrixG;
    }

    public int[][][] getMatrixG() {
        return matrixG;
    }

    public int[] getMultiplyLine(final int[][] matrixG1, final int[] indexes) {
        int[] result = matrixG1[indexes[0]];

        for (int i = 1; i < indexes.length; i++) {
            result = componentMultiply(result, matrixG1[indexes[i]]);
        }

        return result;
    }

    /**
     * Покомпонентное умножение векторов.
     *
     * @param arrA
     * @param arrB
     * @return
     */
    public int[] componentMultiply(final int[] arrA, final int[] arrB) {
        if (arrA.length != arrB.length) {
            throw new IllegalArgumentException("Размерности векторов не совпадают");
        }
        final int[] arrResult = new int[arrA.length];
        for (int i = 0; i < arrResult.length; i++) {
            arrResult[i] = arrA[i] * arrB[i];
        }
        return arrResult;
    }


}
