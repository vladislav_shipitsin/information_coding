package normalRealise.algoritm.read_maller;

import normalRealise.algoritm.Algorithm;
import normalRealise.algoritm.AlgoritmFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadMallerFactory implements AlgoritmFactory {
    @Override
    public Algorithm getAlgoritm(int T, double p, double w) {
        final int n = getBestN(T,p,w);
        final int k = nToK.get(n);
        final int m = k - 1;
        final int r = 1;

        ReadMallerCoder coder = new ReadMallerCoder(k, n, r, m);

        final int[][][] matrixG = coder.getMatrixG();

        ReadMallerDecoder decoder = new ReadMallerDecoder(k, n, r, m, matrixG);

        return new ReadMallerAlgorithm(k, n, coder, decoder);
    }

    public ReadMallerAlgorithm getAlgoritm(final int sizeInfoWord) {
        final int m = sizeInfoWord - 1;
        final int n = (int) Math.pow(2,m);
        final int r = 1;
        final int k = sizeInfoWord;

        ReadMallerCoder coder = new ReadMallerCoder(k, n, r, m);

        final int[][][] matrixG = coder.getMatrixG();

        ReadMallerDecoder decoder = new ReadMallerDecoder(k, n, r, m, matrixG);

        return new ReadMallerAlgorithm(k, n, coder, decoder);
    }
    private static final List<Integer> respectValuesN = new ArrayList<>();
    private static final Map<Integer, Integer> nToK = new HashMap<>();
    private static final Map<Integer, Integer> nToError = new HashMap<>();

    static {
        respectValuesN.add(8);
        respectValuesN.add(16);
        respectValuesN.add(32);
        respectValuesN.add(64);
        respectValuesN.add(128);

        nToK.put(8, 4);
        nToK.put(16, 5);
        nToK.put(32, 6);
        nToK.put(64, 7);
        nToK.put(128, 8);

        nToError.put(8, 1);
        nToError.put(16, 3);
        nToError.put(32, 7);
        nToError.put(64, 15);
        nToError.put(128, 31);
    }

    private static final int MIN_N = 8;
    private static final int MAX_N = 64;
    private static int getCorrectBoundN(final int T) {
        if (T < MIN_N) {
            throw new IllegalArgumentException("Сличком мальнькое T");
        }
        if (MAX_N < T) return MAX_N;
        for (int i = 1; i < respectValuesN.size(); i++) {
            if (respectValuesN.get(i) > T) {
                return respectValuesN.get(i - 1);
            }
        }
        throw new IllegalArgumentException("Ты опять накосячил в логике");
    }

    private static int getBestN(final int T, final double p, final double w) {

        if(T > 30) return 128;

        int boundN = getCorrectBoundN(T);

        double bestProbability = -5;
        int bestN = -1;


        for (Integer n : respectValuesN) {

            if (n > boundN) {
                return bestN;
            }

            double sum = 0.0;
            for (int e = 0; e <= nToError.get(n); e++) {
                sum += bernuli(n, e, p);
            }
            if (sum >= 1 - w) {
                return n;
            }
            if(bestProbability < sum){
                bestProbability = sum;
                bestN = n;
            }
        }

        if(bestN == -1){
            throw new IllegalArgumentException("Чтото не так");
        }
        return bestN;
    }

    private static double bernuli(int n, int k, double p) {
        return getCombination(k, n) * Math.pow(p, k) * Math.pow((1 - p), (n - k));
    }

    public static int getCombination(final int r, final int m) {
        return factorial(m) / factorial(r) / factorial(m - r);
    }

    public static int factorial(final int n) {
        if(n < 0 ) throw new IllegalArgumentException();
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }
}
