package normalRealise.experiment;

import normalRealise.noise.Interference;

public class Parameters {

    private final int T; // длина информационного слова.
    private final double p; // длина информационного слова.
    private final double w; // длина информационного слова.
    private final int numIteration; // количество сообщений.
    private final Interference interference; // белый шум.

    public Parameters(int t, double p, double w, int numIteration, Interference interference) {
        T = t;
        this.p = p;
        this.w = w;
        this.numIteration = numIteration;
        this.interference = interference;
    }


    public Interference getDistortion() {
        return interference;
    }

    public int getT() {
        return T;
    }

    public double getP() {
        return p;
    }

    public double getW() {
        return w;
    }

    public Interference getInterference() {
        return interference;
    }

    public int getNumIteration() {
        return numIteration;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Parameters : \n");
        sb.append("T := ").append(T).append("\n");
        sb.append("numIteration := ").append(numIteration).append("\n");
        sb.append("P := ").append(p).append("\n");
        sb.append("W := ").append(w).append("\n");
        sb.append(interference).append("\n");
        return sb.toString();
    }
}
