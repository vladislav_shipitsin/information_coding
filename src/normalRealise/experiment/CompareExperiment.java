package normalRealise.experiment;

import normalRealise.algoritm.Adamara.AdamaraFactory;
import normalRealise.algoritm.AlgoritmFactory;
import normalRealise.algoritm.hamming.HamingFactory;
import normalRealise.algoritm.read_maller.ReadMallerFactory;
import normalRealise.noise.Interference;
import normalRealise.noise.RandomProbabilityInterference;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Сравнение алгоритмов
 * <p>
 * слова можно разбивать по разному по блоки с этим надо поиграть
 */
public class CompareExperiment {
    private static final Random RANDOM = new Random();


    /**
     * Ограничения задаюстся максимально возможной длинной слова которые мы можем отправить
     * Это N или sizeCodeWord
     * По этому параметру и вероятности ошибке в передаче
     * Мы для каждого алгоритма должны определить наилучние параметры для построения
     *
     * @param args
     */
    public static void main(String[] args) {

        //TODO : ВЫВЕСТИ ПРИ КАКИХ ЗНАЧЕНИХ АЛГОРИТМА, КАКИЕ ПАРАМЕТРЫ ПРИМЕНЯЮТСЯ

        final int sizeInfoWord = 8;
        final int sizeCodeWord = 10;
        final int numIteration = 100;

        final int sizeMessage = 10;

        final Interference interference = new RandomProbabilityInterference(0.01);

       /* final Parameters parameters =
                new Parameters(sizeInfoWord, numIteration, sizeMessage, distortion);*/


        final List<AlgoritmFactory> factories = new LinkedList<>();
//        factories.add(new HamingFactory());
        factories.add(new AdamaraFactory());
        factories.add(new ReadMallerFactory());

        final List<Parameters> parameters = getParameters();
        for(final Parameters parameter : parameters){
            final Experiment experiment = new Experiment(parameter, factories.toArray(AlgoritmFactory[]::new));
            experiment.spend();
        }
    }

    /**
     * Кросс валидация по 3 параментрам
     * 1) Длина информационного слова
     * 2) Длина сообщения
     * 3) Вероятность ошибки в бите
     * 4) Количество итераций или количесво сообщений
     *
     * @return
     */
   /* public static List<Parameters> getParameters() {
        List<Parameters> parameters = new LinkedList<>();

        // все возможные варианты длин кодовых слов.
//        final List<Integer> sizesInfoWord = Arrays.asList(3, 4, 5, 6, 7, 8, 9, 10);
        final List<Integer> sizesInfoWord = Arrays.asList(8);

//        final List<Integer> sizesMessage = Arrays.asList(10, 20, 40, 80, 100);
        final List<Integer> sizesMessage = Arrays.asList(100,10);
        // Больше 0,5 нет смысла брать так как тогда можно инвертировать все биты и считать что вероятность < 0.5
//        final List<Double> probabilities = Arrays.asList(0.01, 0.05, 0.1, 0.2, 0.4, 0.5);
        final List<Double> probabilities = Arrays.asList(0.01);

//        final List<Integer> iterations = Arrays.asList(10, 25, 50, 75, 100);
        final List<Integer> iterations = Arrays.asList(10,100);

        for (final Integer infoWord : sizesInfoWord) {
            for (final Integer message : sizesMessage) {
                for (final Double prob : probabilities) {
                    for (final Integer iter : iterations) {
                        parameters.add(
                                new Parameters(
                                        infoWord,
                                        iter,
                                        message,
                                        new RandomProbabilityInterference(prob)
                                ));
                    }
                }
            }
        }

        return parameters;
    }*/

    public static List<Parameters> getParameters() {
        List<Parameters> parameters = new LinkedList<>();

        // все возможные варианты длин кодовых слов.
//        final List<Integer> sizesInfoWord = Arrays.asList(3, 4, 5, 6, 7, 8, 9, 10);
        final List<Integer> listT= Arrays.asList(32);
        // Больше 0,5 нет смысла брать так как тогда можно инвертировать все биты и считать что вероятность < 0.5
        final List<Double> probabilities = Arrays.asList(0.01, 0.05, 0.1, 0.2, 0.3);
//        final List<Double> probabilities = Arrays.asList(0.2);

//        final List<Double> listW = Arrays.asList(0.1,0.05,0.025,0.01,0.001);
        final List<Double> listW = Arrays.asList(0.01);

        for (final Integer T : listT) {
            for (final Double w : listW) {
                for (final Double prob : probabilities) {

                        parameters.add(
                                new Parameters(
                                        T,
                                        prob,
                                        w,
                                        10_000,
                    new RandomProbabilityInterference(prob)
                                ));

                }
            }
        }

        return parameters;
    }
}
