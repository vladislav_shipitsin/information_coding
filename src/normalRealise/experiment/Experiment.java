package normalRealise.experiment;

import normalRealise.Channel;
import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.AlgoritmFactory;

import java.util.*;

public class Experiment {
    private static final Random RANDOM = new Random();

    private final Parameters parameters;
    private final AlgoritmFactory[] factories;
    private final List<String> messages;
    private final Map<Algorithm , ResultExperiment> resultExperiments;

    public Experiment(Parameters parameter, final AlgoritmFactory... factories) {
        this.parameters = parameter;
        this.factories = factories;
        messages = getGenerateMessage();
        resultExperiments = new HashMap<>();
    }

    public void spend() {

        System.out.println("\nParameters := " + parameters);

        // Время инициализации
        for (final AlgoritmFactory factory : factories) {
            final Algorithm  algoritm = spendInit(factory);
            spendMessages(algoritm);
        }

        printResult();
    }

    private Algorithm  spendInit(final AlgoritmFactory factory) {
        System.out.println("start init :" + factory.getClass().getSimpleName());
        final Time timeInit = new Time();
        timeInit.start();

        final Algorithm  algoritm = factory.getAlgoritm(parameters.getT(),parameters.getP(), parameters.getW());

        timeInit.stop();

        final ResultExperiment resultExperiment = new ResultExperiment(algoritm, parameters);
        resultExperiment.setTimeInit(timeInit);
        resultExperiments.put(algoritm, resultExperiment);

        System.out.println("finish init :" + factory.getClass().getSimpleName());
        return algoritm;
    }

    //  Для чистоты эксперимета будем посылать одинаковые сообщения.
    private void spendMessages(final Algorithm  algoritm) {
        System.out.println("start spend message :" + algoritm.getClass().getSimpleName());

        final ResultExperiment resultExperiment = resultExperiments.get(algoritm);
        final Time timeSpend = new Time();
        timeSpend.start();
        int numSuccessfully = 0;

        final Channel channel = new Channel(algoritm, parameters.getDistortion());

        for (final String message : messages) {

            final String actual = channel.processMessage(message);

            if (message.equals(actual)) {
                numSuccessfully++;
            }
        }
        timeSpend.stop();

        resultExperiment.setTimeSpend(timeSpend);
        resultExperiment.setNumSuccessfully(numSuccessfully);

        System.out.println("finish spend message :" + algoritm.getClass().getSimpleName());
    }

    private List<String> getGenerateMessage() {
        final List<String> messages = new LinkedList<>();

        for (int i = 0; i < parameters.getNumIteration(); i++) {
            final String message = generateMessage(100);
            messages.add(message);
        }
        return messages;
    }

    private String generateMessage(final int size) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append((char) (RANDOM.nextInt(25) + 65));
        }
        return sb.toString();
    }

    private void printResult() {

        resultExperiments.values().forEach(ResultExperiment::print);
    }
}
