package normalRealise.experiment;

import normalRealise.algoritm.Algorithm ;

public class ResultExperiment {
    private final Algorithm  algoritm;
    private final Parameters parameters;

    private Time timeInit;
    private Time timeSpend;

    private long numSuccessfully;

    public ResultExperiment(Algorithm  algoritm, Parameters parameters) {
        this.algoritm = algoritm;
        this.parameters = parameters;
    }

    public void setTimeInit(Time timeInit) {
        this.timeInit = timeInit;
    }

    public void setTimeSpend(Time timeSpend) {
        this.timeSpend = timeSpend;
    }

    public void setNumSuccessfully(long numSuccessfully) {
        this.numSuccessfully = numSuccessfully;
    }

    public void print() {
        StringBuilder sb = new StringBuilder();
        sb.append("------------------------------------------------------------------").append("\n");;
        sb.append(algoritm).append("\n");
        sb.append("init :=").append(timeInit).append("\n");
        sb.append("spend :=").append(timeSpend).append("\n");
        sb.append("numSuccessfully := ").append(numSuccessfully).append("\n");
        sb.append("Successfully := ")
                .append(100 * numSuccessfully / parameters.getNumIteration())
                .append("%")
                .append("\n");
        sb.append("------------------------------------------------------------------");
        System.out.println(sb.toString());
    }
}
