package normalRealise.experiment;

public class Time {
    private long start;
    private long finish;
    private long difference;

    public Time() {
    }

    public void start() {
        start = now();
        finish = 0;
        difference = 0;
    }

    public void stop() {
        finish = now();
        difference = finish - start;
    }

    public long getDifference() {
        return difference;
    }

    private long now(){
        return System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "time = " + difference + "ms";
    }
}
