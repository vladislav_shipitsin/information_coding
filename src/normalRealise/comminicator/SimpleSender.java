package normalRealise.comminicator;

import normalRealise.algoritm.Algorithm ;

import java.util.LinkedList;
import java.util.List;

/**
 * Отправитель сообщений
 */
public class SimpleSender implements Sender {

    @Override
    public List<String> send(final Algorithm  algoritm, final String allInfoWord) {
        final List<String> codeWords = new LinkedList<>();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < allInfoWord.length(); i++) {

            sb.append(allInfoWord.charAt(i));

            if (sb.length() == algoritm.getSizeInfoWord()) {
                final String infoWord = sb.toString();
                final String codeWord = algoritm.getCoder().coding(infoWord);
                codeWords.add(codeWord);
                sb = new StringBuffer();
            }
        }

        if (sb.length() > 0 && sb.length() < algoritm.getSizeInfoWord()) {
            while (sb.length() < algoritm.getSizeInfoWord()) {
                sb.append('0');
            }
            final String infoWord = sb.toString();
            final String codeWord = algoritm.getCoder().coding(infoWord);
            codeWords.add(codeWord);
        }
        return codeWords;
    }
}
