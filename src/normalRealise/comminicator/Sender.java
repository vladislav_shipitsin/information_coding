package normalRealise.comminicator;

import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.Coder;

import java.util.List;

/**
 * Отправитель сообщений
 */
public interface Sender {
    /**
     * По сообщению получить список информационных слов закодированных кодером.
     * @param algoritm
     * @param allInfoWord
     * @return
     */
    List<String> send(final Algorithm  algoritm, String allInfoWord);
}
