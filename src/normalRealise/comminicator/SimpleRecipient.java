package normalRealise.comminicator;

import normalRealise.algoritm.Algorithm ;

import java.util.List;

public class SimpleRecipient implements Recipient {

    @Override
    public String process(final Algorithm  algoritm, final List<String> distortedWords) {
        final StringBuffer resultInfoWord = new StringBuffer();

        for (final String distWord : distortedWords) {

            final String infoWord = algoritm.getDecoder().decoding(distWord);
            resultInfoWord.append(infoWord);
        }

        return resultInfoWord.toString();
    }
}
