package normalRealise.comminicator;

import normalRealise.algoritm.Algorithm ;

import java.util.List;

/**
 * Получатель сообщений.
 */
public interface Recipient {

    String process(final Algorithm  algoritm, final List<String> distortedWords);
}
