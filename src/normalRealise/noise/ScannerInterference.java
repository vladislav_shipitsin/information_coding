package normalRealise.noise;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScannerInterference extends Interference {
    @Override
    public String imposeNoise(String codeWord) {
        Scanner scanner = new Scanner(System.in);
        final int index = scanner.nextInt();
        return changeElement(codeWord,index);
    }

    @Override
    public List<Integer> getIndexes(final int maxIndex) {
        System.out.println("enter indexes for change");
        Scanner scanner = new Scanner(System.in);
        final int index = scanner.nextInt();
        return new ArrayList<>(index);
    }
}
