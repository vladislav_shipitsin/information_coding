package normalRealise.noise;

import java.util.Arrays;
import java.util.List;

public class SimpleInterference extends Interference {

    private List<Integer> indexes;

    public SimpleInterference(Integer... indexes) {
        this.indexes = Arrays.asList(indexes);
    }

    @Override
    public List<Integer> getIndexes(final int maxIndex) {
        return indexes;
    }
}
