package normalRealise.noise;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * С вероятностью p меняет каждый элемент в слове.
 */
public class RandomProbabilityInterference extends Interference {
    private static final Random RANDOM = new Random();
    private final double probability;

    public RandomProbabilityInterference(final double probability) {
        this.probability = getCorrectProb(probability);
    }

    private double getCorrectProb(final double probability){
        if(probability < 0.0) return 0.0;
        return Math.min(probability, 1.0);
    }

    @Override
    public List<Integer> getIndexes(int maxIndex) {
        final List<Integer> indexesChange = new LinkedList<>();
        for (int i = 0; i < maxIndex; i++) {
            final double currentRand = RANDOM.nextDouble();
            if(probability > currentRand){
                indexesChange.add(i);
            }
        }
        return indexesChange;
    }

    @Override
    public String toString() {
        final String prefix = super.toString();
        return prefix + "prob := " + probability;
    }
}
