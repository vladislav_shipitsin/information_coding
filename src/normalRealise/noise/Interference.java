package normalRealise.noise;

import java.util.List;

/**
 * Искажатель информации
 */
public abstract class Interference {

    public String imposeNoise(final String codeWord) {
        String resultWord = codeWord;
        for (Integer index : getIndexes(codeWord.length())) {
            resultWord = changeElement(resultWord, index);
        }
        return resultWord;
    }

    String changeElement(final String codeWord, final int index) {
        final StringBuffer sb = new StringBuffer(codeWord);
        sb.setCharAt(index, sb.charAt(index) == '0' ? '1' : '0');
        return sb.toString();
    }

    public abstract List<Integer> getIndexes(final int maxIndex);

    @Override
    public String toString() {
        return "Distortion := " + getClass().getSimpleName();
    }
}
