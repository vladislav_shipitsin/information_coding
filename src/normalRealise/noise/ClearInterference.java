package normalRealise.noise;

import java.util.ArrayList;
import java.util.List;

public class ClearInterference extends Interference {
    /**
     * Не накладывает шум.
     * @param codeWord
     * @return
     */
    @Override
    public String imposeNoise(String codeWord) {
        return codeWord;
    }

    @Override
    public List<Integer> getIndexes(final int maxIndex) {
        return new ArrayList<>();
    }
}
