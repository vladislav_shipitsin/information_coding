package normalRealise.noise;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * С вероятностью 1 меняет ровно 1 элемент в слове.
 */
public class RandomIndexesInterference extends Interference {
    private static final Random RANDOM = new Random();

    @Override
    public List<Integer> getIndexes(final int maxIndex) {
        return Collections.singletonList(RANDOM.nextInt(maxIndex));
    }


}
