package test;

import normalRealise.Channel;
import normalRealise.algoritm.Adamara.AdamaraFactory;
import normalRealise.algoritm.Algorithm ;
import normalRealise.noise.RandomIndexesInterference;
import normalRealise.noise.SimpleInterference;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class BigRandomTestAdamara {
    private static final int SIZE_INFO_WORD = 14;
    private static final Random RANDOM = new Random();
    private static final Algorithm  ADAMARA = new AdamaraFactory().getAlgoritm(SIZE_INFO_WORD);

    public String generateMessage(final int size) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < size; i++) {
            sb.append((char) (RANDOM.nextInt(25) + 65));
        }

        return sb.toString();
    }

    @Test
    public void testGenerate() {
        final String actual = generateMessage(10);

        assertEquals(10, actual.length());
        System.out.println(actual);
    }

    @RepeatedTest(10000)
    public void test() {
        System.out.println("-------------------------START------------------------------------");
        final String expectedMessage = generateMessage(40);
        System.out.println("expected : " + expectedMessage);

        Channel channel = new Channel(ADAMARA, new SimpleInterference(1,2,6));

        final String actualMessage = channel.processMessage(expectedMessage);

        assertEquals(expectedMessage, actualMessage);
        System.out.println("actual : " + actualMessage);
        System.out.println("-------------------------END------------------------------------");
    }

    @Test
    public void testTwoError(){
        final String expectedMessage = "as";
        Channel channel = new Channel(ADAMARA, new SimpleInterference(1,2,3,4,5,6,7,8,9,10,11,12,13));

        final String actualMessage = channel.processMessage(expectedMessage);

        assertEquals(expectedMessage, actualMessage);
    }
}
