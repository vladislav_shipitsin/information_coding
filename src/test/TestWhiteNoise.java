package test;

import main.noise.WhiteNoise;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestWhiteNoise {
    @Test
    public void testImpose(){
        String code = "11001101";
        int[] indexs = {0,1,7,4,5,2};
        String expected = "00100000";
        String result = WhiteNoise.impose(code,indexs);
        assertEquals(expected,result);
    }
}
