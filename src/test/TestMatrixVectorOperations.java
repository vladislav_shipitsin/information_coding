package test;

import main.helpClasses.MatrixVectorOperations;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestMatrixVectorOperations {
    @Test
    public void test(){

        final int[][] matrix = {{1,2},{4,5},{7,8}};
        final int[] arr = {1,2,3};

        final int[] actual = MatrixVectorOperations.multiplyArrToMatrix(arr,matrix);

        System.out.println(Arrays.toString(actual));

    }

    @Test
    public void test2(){

        final int[][] matrix = {{1,1},{0,1},{1,0}};
        final int[] arr = {1,0,1};

        final int[] actual = MatrixVectorOperations.multiplyArrToMatrix(arr,matrix);

        System.out.println(Arrays.toString(actual));

    }
}
