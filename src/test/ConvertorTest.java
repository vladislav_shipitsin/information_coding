package test;

import main.helpClasses.Convertor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConvertorTest {

    @Test
    public void testEng(){
        String hello = "hello";
        String expected = "0110100001100101011011000110110001101111";
        String result = Convertor.convertStringToCode(hello);
        assertTrue(result.length() % 8 == 0);
        assertEquals(result.length(),40);
        assertEquals(expected,result);
    }
    @Test
    public void testConvertCodeToString(){
        String code = "0110100001100101011011000110110001101111";
        String expected = "hello";
        String result = Convertor.convertCodeToString(code);
        assertEquals(expected,result);
    }
    @Test
    public void testChar(){
        String hello = "a";
        String expected = "0110100001100101011011000110110001101111";
        String result = Convertor.convertStringToCode(hello);
        System.out.println("result = " + result);
        assertTrue(result.length() % 8 == 0);
        assertEquals(result.length(),40);
        assertEquals(expected,result);
    }

}
