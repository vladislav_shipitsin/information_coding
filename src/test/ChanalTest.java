package test;

import normalRealise.Channel;
import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.hamming.HamingFactory;
import normalRealise.comminicator.SimpleSender;
import normalRealise.noise.ClearInterference;
import normalRealise.noise.RandomIndexesInterference;
import normalRealise.noise.SimpleInterference;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChanalTest {

    private static final BiFunction<Integer, Integer, Algorithm > GET_SIMPLE_ALGORITME =
            (sizeInfoWord, sizeCodeWord) ->
                    new Algorithm (
                            sizeInfoWord,
                            sizeCodeWord,
                            infoWord -> null,
                            distortedWord -> distortedWord) {
                    };

    @Test
    public void test1() {
        SimpleSender sender = new SimpleSender();
        int sizeInfoWord = 3;
        int sizeCodeWord = 7;

        Algorithm  simpleAlgoritm = GET_SIMPLE_ALGORITME.apply(sizeInfoWord, sizeCodeWord);

        List<String> actual = sender.send(simpleAlgoritm, "123456");

        assertEquals(2, actual.size());
        assertEquals("123", actual.get(0));
        assertEquals("456", actual.get(1));
    }

    @Test
    public void test2() {
        SimpleSender sender = new SimpleSender();
        int sizeInfoWord = 3;
        int sizeCodeWord = 7;

        Algorithm  simpleAlgoritm = GET_SIMPLE_ALGORITME.apply(sizeInfoWord, sizeCodeWord);


        List<String> actual = sender.send(simpleAlgoritm, "1234567");

        assertEquals(3, actual.size());
        assertEquals("123", actual.get(0));
        assertEquals("456", actual.get(1));
        assertEquals("700", actual.get(2));
    }

    @Test
    public void test3() {
        SimpleSender sender = new SimpleSender();
        int sizeInfoWord = 10;
        int sizeCodeWord = 12;

        Algorithm  simpleAlgoritm = GET_SIMPLE_ALGORITME.apply(sizeInfoWord, sizeCodeWord);


        List<String> actual = sender.send(simpleAlgoritm, "1234567");

        assertEquals(1, actual.size());
        assertEquals("1234567000", actual.get(0));

    }

    @Test
    public void testRecipient() {
        final String message = "hello";

        int sizeInfoWord = 3;
        int sizeCodeWord = 7;
        Algorithm  simpleAlgoritm = GET_SIMPLE_ALGORITME.apply(sizeInfoWord, sizeCodeWord);

        Channel channel = new Channel(simpleAlgoritm, new ClearInterference());

        final String actual = channel.processMessage(message);

        assertEquals(message, actual);
    }

    @Test
    public void testAdamara() {
        final String message = "a";

        int sizeInfoWord = 3;

        Algorithm  algoritm = new HamingFactory().getAlgoritm(sizeInfoWord);

        Channel channel = new Channel(algoritm, new SimpleInterference(1));

        final String actual = channel.processMessage(message);

        assertEquals(message, actual);
    }

    @Test
    public void testAdamaraHello() {
        final String message = "Hello";

        int sizeInfoWord = 5;

        Algorithm  algoritm = new HamingFactory().getAlgoritm(sizeInfoWord);

        Channel channel = new Channel(algoritm, new SimpleInterference(1));

        final String actual = channel.processMessage(message);

        assertEquals(message, actual);
    }

    @Test
    public void testAdamaraHelloRandom() {
        final String message = "Hello";

        int sizeInfoWord = 5;

        Algorithm  algoritm = new HamingFactory().getAlgoritm(sizeInfoWord);

        Channel channel = new Channel(algoritm, new RandomIndexesInterference());

        final String actual = channel.processMessage(message);

        assertEquals(message, actual);
    }
}
