package test;

import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.read_maller.ReadMallerFactory;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class TestReadMaller {
    @Test
    public void testSimple() {

        int k = 4;
        int n = 8;
        int r = 1;
        int m = 3;

        Algorithm  algoritm = new ReadMallerFactory().getAlgoritm(4);

        final String message = "0110";

        StringBuilder codeWord = new StringBuilder(algoritm.getCoder().coding(message));

        codeWord.replace(2, 3, "0");

        String actual = algoritm.getDecoder().decoding(codeWord.toString());

        assertEquals(message, actual);
    }

    @Test
    public void testSimpleBig() {

        int k = 11;
        int n = 16;
        int r = 2;
        int m = 4;

        final String message = "11110010011";

        Algorithm  algoritm = new ReadMallerFactory().getAlgoritm(11);

        StringBuilder codeWord = new StringBuilder(algoritm.getCoder().coding(message));

        codeWord.replace(0, 1, "0");

        String actual = algoritm.getDecoder().decoding(codeWord.toString());

        assertEquals(message, actual);
    }
}
