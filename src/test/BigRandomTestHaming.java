package test;

import normalRealise.Channel;
import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.hamming.HamingFactory;
import normalRealise.noise.RandomProbabilityInterference;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class BigRandomTestHaming {
    private static final int SIZE_INFO_WORD = 3;
    private static final Random RANDOM = new Random();
    private static final Algorithm  HAMING = new HamingFactory().getAlgoritm(SIZE_INFO_WORD);

    public String generateMessage(final int size) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < size; i++) {
            sb.append((char) (RANDOM.nextInt(25) + 65));
        }

        return sb.toString();
    }

    @Test
    public void testGenerate() {
        final String actual = generateMessage(15);

        assertEquals(10, actual.length());
        System.out.println(actual);
    }

    @RepeatedTest(10000)
    public void test() {
        System.out.println("-------------------------START------------------------------------");
        final String expectedMessage = generateMessage(8);
        System.out.println("expected : " + expectedMessage);

        Channel channel = new Channel(new HamingFactory().getAlgoritm(20,0.1,0.1), new RandomProbabilityInterference(0.01));

        final String actualMessage = channel.processMessage(expectedMessage);

        assertEquals(expectedMessage, actualMessage);
        System.out.println("actual : " + actualMessage);
        System.out.println("-------------------------END------------------------------------");
    }

    @Test
    public void testFor() {
        int succes = 0;
        for (int i = 0; i < 10000; i++) {
            System.out.println("-------------------------START------------------------------------");
            final String expectedMessage = generateMessage(8);
            System.out.println("expected : " + expectedMessage);

            Channel channel = new Channel(new HamingFactory().getAlgoritm(SIZE_INFO_WORD), new RandomProbabilityInterference(0.1));

            final String actualMessage = channel.processMessage(expectedMessage);

            if(expectedMessage.equals(actualMessage)){
                succes++;
            }
            System.out.println("actual : " + actualMessage);
            System.out.println("-------------------------END------------------------------------");
        }
        System.out.println(succes);
    }
}
