package test;

import normalRealise.Channel;
import normalRealise.algoritm.Algorithm ;
import normalRealise.algoritm.read_maller.ReadMallerFactory;
import normalRealise.noise.RandomIndexesInterference;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class BigRandomTestReadMaller {
    private static final int SIZE_INFO_WORD = 8;
    private static final Random RANDOM = new Random();
    private static final Algorithm  READ_MALLER = new ReadMallerFactory().getAlgoritm(4);

    public String generateMessage(final int size) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < size; i++) {
            sb.append((char) (RANDOM.nextInt(25) + 65));
        }

        return sb.toString();
    }

    @Test
    public void testGenerate() {
        final String actual = generateMessage(15);

        assertEquals(10, actual.length());
        System.out.println(actual);
    }

    @RepeatedTest(10000)
    public void test() {
        System.out.println("-------------------------START------------------------------------");
        final String expectedMessage = generateMessage(8);
        System.out.println("expected : " + expectedMessage);

        Channel channel = new Channel(READ_MALLER, new RandomIndexesInterference());

        final String actualMessage = channel.processMessage(expectedMessage);

        assertEquals(expectedMessage, actualMessage);
        System.out.println("actual : " + actualMessage);
        System.out.println("-------------------------END------------------------------------");
    }

}
